package ru.sbt.mp2017.game_of_life;


public interface GameOfLife {
    void randomSeed();

    void iterate();
}
