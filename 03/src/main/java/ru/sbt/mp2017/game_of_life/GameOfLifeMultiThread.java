package ru.sbt.mp2017.game_of_life;


import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GameOfLifeMultiThread implements GameOfLife {

    private final int width;
    private final int height;
    private final int size;
    private final int numExecutors;
    private int seedCount = 100;
    private byte[] data;


    public GameOfLifeMultiThread() {
        this(100, 100);
    }

    public GameOfLifeMultiThread(int width, int height) {
        this.width = width;
        this.height = height;
        this.size = width * height;
        data = new byte[size];
        this.numExecutors = 10;
    }


    @Override
    public void iterate() {

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.execute(new Runnable() {
            public void run() {
                System.out.println("Asynchronous task");
            }
        });

        executorService.shutdown();
        byte[] prev = new byte[size];
        System.arraycopy(data, 0, prev, 0, size);

        int blockSize = height / numExecutors;
        byte[] next = new byte[size];
        for (int i = 0; i < width; i++) {
                executorService.execute(() -> {
                for (int i1 = 0; i1 < width; i1++) {
                    for (int j = 0; j < height; j++) {
                        int type = isAlive(i1, j, prev);

                        if (type > 0) {
                            next[j * width + i1] = 1;
                        } else {
                            next[j * width + i1] = 0;
                        }
                    }
                }
            });
        }
//        // waiting until all workers ended their work
//        while (true) {
//            if (executorService.isTerminated()) {
//                break;
//            }
//        }
        System.arraycopy(next, 0, data, 0, size);
    }

    private int isAlive(int x, int y, byte[] d) {
        int count = 0;
        int pos1 = y * width + x;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                int pos = j * width + i;
                if (pos >= 0 && pos < size - 1 && pos != pos1) {
                    if (d[pos] == 1) {
                        count++;
                    }
                }
            }
        }
        //dead
        if (d[pos1] == 0) {
            if (count == 3) {//becomes alive.
                return 1;
            }
            return 0;//still dead
        } else {//live
            if (count < 2 || count > 3) {//Dies
                return 0;
            }
            return 1;//lives
        }
    }

    @Override
    public void randomSeed() {
        for (int i = 0; i < seedCount; i++) {
            int x = (int) (Math.random() * width);
            int y = (int) (Math.random() * height);
            data[y * width + x] = 1;
        }
    }

    public static void main(String[] args) {
        GameOfLife gameOfLife = new GameOfLifeMultiThread(10, 10);
        gameOfLife.randomSeed();
        gameOfLife.iterate();
    }
}
