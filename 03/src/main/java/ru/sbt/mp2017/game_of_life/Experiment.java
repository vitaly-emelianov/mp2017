package ru.sbt.mp2017.game_of_life;

import java.util.ArrayList;
import java.util.List;


public class Experiment {

    List<Integer> sizes = new ArrayList<Integer>() {{
        add(10);
        add(100);
        add(200);
        add(300);
        add(400);
        add(500);
        add(600);
        add(700);
        add(800);
        add(900);
        add(1000);
    }};

    public void runSingleThread() {
        for (Integer size : sizes) {
            long startTime = System.nanoTime();
            GameOfLife gameOfLifeSingleThread = new GameOfLifeSingleThread(size, size);
            gameOfLifeSingleThread.randomSeed();
            for (int i = 0; i < 100; i++) {
                gameOfLifeSingleThread.iterate();
            }
            System.out.println(String.format("%d %d", size, System.nanoTime() - startTime));
        }
    }


    public void runMultiThread() {
        for (Integer size : sizes) {
            long startTime = System.nanoTime();
            GameOfLife gameOfLifeSingleThread = new GameOfLifeSingleThread(size, size);
            gameOfLifeSingleThread.randomSeed();
            for (int i = 0; i < 100; i++) {
                gameOfLifeSingleThread.iterate();
            }
            System.out.println(String.format("%d %d", size, System.nanoTime() - startTime));
        }
    }


    public static void main(String[] args) {
        Experiment experiment = new Experiment();
        experiment.runMultiThread();
    }
}
