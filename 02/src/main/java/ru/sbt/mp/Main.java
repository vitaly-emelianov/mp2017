package ru.sbt.mp;


import ru.sbt.mp.token_ring.TokenRing;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        TokenRing tokenRing = new TokenRing(100);
        tokenRing.runExperiment(1000);
    }
}
