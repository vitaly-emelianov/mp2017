package ru.sbt.mp.token_ring;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TokenRing {

    private List<Node> nodes = new ArrayList<>();
    private int numberOfNodes;

    public TokenRing(int numberOfNodes) throws IOException {
        this.numberOfNodes = numberOfNodes;
        Node head = new Node(0, null);
        nodes.add(head);
        Node node = head;
        for (int i = 1; i < numberOfNodes; i++) {
            Node newNode = new Node(i, node);
            nodes.add(newNode);
            node = newNode;
        }
        head.setNext(node);
    }

    public void runExperiment(int numberOfMessages) {
        for (Node node : nodes) {
            node.start();
        }
        Random random = new Random();
        for (int i = 0; i < numberOfMessages; i++) {
            System.out.println(i);
            int randomReceiver = random.nextInt(numberOfNodes);
            int randomAddress = random.nextInt(numberOfNodes);
            Message message = new Message(randomAddress);
            nodes.get(randomReceiver).process(message);
        }
        for (Node node : nodes) {
            node.stopNode();
        }
    }
}
