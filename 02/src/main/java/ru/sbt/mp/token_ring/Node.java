package ru.sbt.mp.token_ring;


import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;


public class Node extends Thread {

    private Node next;
    private int index;
    private NodeState state;
    private Deque<Message> deque = new ConcurrentLinkedDeque<Message>();
    private Logger logger;

    public Node(int index, Node next) throws IOException {
        this.index = index;
        this.next = next;
        state = NodeState.RUNNING;
        logger = Logger.getLogger(Integer.toString(index));

    }

    public void process(Message message) {
        if (index != message.getAddress()) {
            next.add(message);
        }
        logger.info(String.format("%s %s %s",
                message.getId(), Integer.toString(index), message.toString()));
    }

    public void add(Message message) {
        this.deque.addFirst(message);
    }

    public void setNext(Node node) {
        this.next = node;
    }

    @Override
    public void run() {
        while (state.equals(NodeState.RUNNING)) {
            if (!deque.isEmpty()) {
                Message message = deque.removeLast();
                process(message);
            }
        }
    }

    public void stopNode() {
        this.state = NodeState.STOPPED;
    }


}
