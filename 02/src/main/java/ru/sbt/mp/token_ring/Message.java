package ru.sbt.mp.token_ring;


import java.util.UUID;

public class Message {
    private final String uuid;
    private final int address;

    public Message(int address) {
        this.address = address;
        uuid = UUID.randomUUID().toString();
    }

    public String getId() {
        return uuid;
    }

    public int getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return Integer.toString(address);
    }
}
